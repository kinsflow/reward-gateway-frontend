import { createRouter, createWebHistory } from 'vue-router'
import Login from '../views/Login.vue';
import Register from '../views/Register.vue';
import UserLogin from '../views/UserLogin.vue';
import Layout from '../components/Layout.vue';
import Products from '../views/products.vue'

const routes = [
  {
    path: '/admin',
    name: 'Login',
    component: Login
  },
  {
    path: '/user-login',
    name: 'UserLogin',
    component: UserLogin
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  },
 
      {
        path: '/add-product',
        name: 'addProduct',
        component: () => import(/* webpackChunkName: "about" */ '../views/addProduct.vue')
      },
      {
        path: '/product-list',
        name: 'Products',
        component: () => import(/* webpackChunkName: "about" */ '../views/products.vue')
      },
      {
        path: '/product-edit',
        name: 'ProductEdit',
        component: () => import(/* webpackChunkName: "about" */ '../views/EditProduct.vue')
      },


  {
    path: '/:catchAll(.*)', redirect: '/user-login'
  } 
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
