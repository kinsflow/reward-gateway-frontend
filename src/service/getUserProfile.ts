export const userDetails = () => {
    const profile = JSON.parse(localStorage.getItem('user_profile') || 'null');
    return profile;
}

export const isAdmin = () => {
    if (userDetails().guard === 'admin') {
        return true
    }
    return false;
}
